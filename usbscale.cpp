#include <stdio.h>
#include <sys/types.h>
#include <stdint.h>
#include <math.h>
#if (defined(_WIN16) || defined(_WIN32) || defined(_WIN64)) && !defined(__WINDOWS__)
#include "portable_endian.h"
#endif
#include <libusb-1.0/libusb.h>
//#define DEBUG
#include "scales.h"
#include <curl.h>
#include <deque>
#include <sstream>
#define WEIGH_REPORT_SIZE 0x06

static libusb_device* find_scale(libusb_device**);
static int print_scale_data(unsigned char*);
uint8_t get_first_endpoint_address(libusb_device* dev);

const char* UNITS[13] = {
    "units",        // unknown unit
    "mg",           // milligram
    "g",            // gram
    "kg",           // kilogram
    "cd",           // carat
    "taels",        // lian
    "gr",           // grain
    "dwt",          // pennyweight
    "tonnes",       // metric tons
    "tons",         // avoir ton
    "ozt",          // troy ounce
    "oz",           // ounce
    "lbs"           // pound
};


CURL *curl;
CURLcode res;

std::deque<int> coffee;

std::string coffee_string() {
	std::ostringstream s;
	for (auto it : coffee) {
		s << it <<',';
	}
	s << 0;
	return s.str();
}

int postdata() {
	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, "http://kahvi.kuolo.fi/update.php");
		std::string c = "pwd=anssikela&data=";
		c += coffee_string();

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, c.c_str());

		res = curl_easy_perform(curl);

		if (res != CURLE_OK)
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
				curl_easy_strerror(res));

		curl_easy_cleanup(curl);
	}
	return 0;
}

void coffee_add(int x) {
	coffee.push_front(x);
	if (coffee.size() > 300) {
		coffee.pop_back();
	}
}

#define MINW 500
#define MAXW 1150

//#define MINW 250.0
//#define MAXW 750.0

int normalize_weight(int a)
{
	int r = static_cast<int>(100 * ((a - MINW) / (MAXW - MINW)));
	return (r > 0)? r : 0;
}

int main(void){

	curl_global_init(CURL_GLOBAL_ALL);

	//for (int i = 0; i < 400; i++) {
	//	coffee_add(i);
	//}
	//postdata();

    libusb_device **devs;
    int r; // holds return codes
    ssize_t cnt;
    libusb_device* dev;
    libusb_device_handle* handle;

    int weigh_count = WEIGH_COUNT -1;

    r = libusb_init(NULL);

    if (r < 0)
        return r;

	#ifdef DEBUG
        libusb_set_debug(NULL, 3);
	#endif
		for (;;) {
    cnt = libusb_get_device_list(NULL, &devs);
    if (cnt < 0)
        return (int) cnt;

    dev = find_scale(devs);
    if(dev == 0) {
        fprintf(stderr, "No USB scale found on this computer.\n");
        return -1;
    }
    
	//-----------------------------------------------------------

    r = libusb_open(dev, &handle);

    if(r < 0) {
        if(r == LIBUSB_ERROR_ACCESS) {
            fprintf(stderr, "Permission denied to scale.\n");
        }
        else if(r == LIBUSB_ERROR_NO_DEVICE) {
            fprintf(stderr, "Scale has been disconnected.\n");
        }
        return -1;
    }

	#ifdef __linux__
		libusb_detach_kernel_driver(handle, 0);
	#endif

    libusb_claim_interface(handle, 0);

	
		unsigned char data[WEIGH_REPORT_SIZE];
		int len;
		int scale_result = -1;

		// For some reason, we get old data the first time, so let's just get that
		// out of the way now. It can't hurt to grab another packet from the scale.
		r = libusb_interrupt_transfer(
			handle,
			//bmRequestType => direction: in, type: class,
					//    recipient: interface
			LIBUSB_ENDPOINT_IN | //LIBUSB_REQUEST_TYPE_CLASS |
			LIBUSB_RECIPIENT_INTERFACE,
			data,
			WEIGH_REPORT_SIZE, // length of data
			&len,
			10000 //timeout => 10 sec
		);

		for (;;) {
			r = libusb_interrupt_transfer(
				handle,
				//bmRequestType => direction: in, type: class,
						//    recipient: interface
				get_first_endpoint_address(dev),
				data,
				WEIGH_REPORT_SIZE, // length of data
				&len,
				10000 //timeout => 10 sec
			);
			// 
			// If the data transfer succeeded, then we pass along the data we
			// received tot **print_scale_data**.
			//
			if (r == 0) {
#ifdef DEBUG
				int i;
				for (i = 0; i < WEIGH_REPORT_SIZE; i++) {
					printf("%x\n", data[i]);
				}
#endif
				if (weigh_count < 1) {
					scale_result = print_scale_data(data);
					if (scale_result != 1)
						break;
				}
				weigh_count--;
			}
			else {
				fprintf(stderr, "Error in USB transfer\n");
				scale_result = -1;
				break;
			}
		}
		postdata();
		libusb_close(handle);
		libusb_free_device_list(devs, 1);
		
		Sleep(2000);
	}

	#ifdef __linux__
		libusb_attach_kernel_driver(handle, 0);
	#endif
    //libusb_close(handle);
    //libusb_free_device_list(devs, 1);
    libusb_exit(NULL);

	curl_global_cleanup();
	return 1;
	//return scale_result;
}

static int print_scale_data(unsigned char* dat) {

    static uint8_t lastStatus = 0;

    uint8_t report = dat[0];
    uint8_t status = dat[1];
    uint8_t unit   = dat[2];
    // Accoring to the docs, scaling applied to the data as a base ten exponent
    int8_t  expt   = dat[3];
    // convert to machine order at all times
    double weight = (double) le16toh(dat[5] << 8 | dat[4]);
    // since the expt is signed, we do not need no trickery
    weight = weight * pow(10, expt);

    //
    // The scale's first byte, its "report", is always 3.
    //
    if(report != 0x03 && report != 0x04) {
        fprintf(stderr, "Error reading scale data\n");
        return -1;
    }

    //
    // Switch on the status byte given by the scale. Note that we make a
    // distinction between statuses that we simply wait on, and statuses that
    // cause us to stop (`return -1`).
    //
    switch(status) {
        case 0x01:
            fprintf(stderr, "Scale reports Fault\n");
            return -1;
        case 0x02:
            if(status != lastStatus)
                fprintf(stderr, "Scale is zero'd...\n");
            break;
        case 0x03:
            if(status != lastStatus)
                fprintf(stderr, "Weighing...\n");
            break;
        //
        // 0x04 is the only final, successful status, and it indicates that we
        // have a finalized weight ready to print. Here is where we make use of
        // the `UNITS` lookup table for unit names.
        //
        case 0x04:
            printf("%g %s\n", weight, UNITS[unit]);
			coffee_add(normalize_weight(static_cast<int>(10 * weight)));
            return 0;
        case 0x05:
            if(status != lastStatus)
                fprintf(stderr, "Scale reports Under Zero\n");
            break;
        case 0x06:
            if(status != lastStatus)
                fprintf(stderr, "Scale reports Over Weight\n");
            break;
        case 0x07:
            if(status != lastStatus)
                fprintf(stderr, "Scale reports Calibration Needed\n");
            break;
        case 0x08:
            if(status != lastStatus)
                fprintf(stderr, "Scale reports Re-zeroing Needed!\n");
            break;
        default:
            if(status != lastStatus)
                fprintf(stderr, "Unknown status code: %d\n", status);
            return -1;
    }
    
    lastStatus = status;
    return 1;
}

static libusb_device* find_scale(libusb_device **devs)
{

    int i = 0;
    libusb_device* dev = 0;

    while ((dev = devs[i++]) != NULL) {
        struct libusb_device_descriptor desc;
        int r = libusb_get_device_descriptor(dev, &desc);
        if (r < 0) {
            fprintf(stderr, "failed to get device descriptor");
            return NULL;
        }
        int i;
        for (i = 0; i < NSCALES; i++) {
            if(desc.idVendor  == scales[i][0] && 
               desc.idProduct == scales[i][1]) {

#ifdef DEBUG

                fprintf(stderr,
                        "Found scale %04x:%04x (bus %d, device %d)\n",
                        desc.idVendor,
                        desc.idProduct,
                        libusb_get_bus_number(dev),
                        libusb_get_device_address(dev));

                    fprintf(stderr,
                            "It has descriptors:\n\tmanufc: %d\n\tprodct: %d\n\tserial: %d\n\tclass: %d\n\tsubclass: %d\n",
                            desc.iManufacturer,
                            desc.iProduct,
                            desc.iSerialNumber,
                            desc.bDeviceClass,
                            desc.bDeviceSubClass);

                    /*
                     * A char buffer to pull string descriptors in from the device
                     */
                    unsigned char string[256];
                    libusb_device_handle* hand;
                    libusb_open(dev, &hand);

                    r = libusb_get_string_descriptor_ascii(hand, desc.iManufacturer,
                            string, 256);
                    fprintf(stderr,
                            "Manufacturer: %s\n", string);
                    libusb_close(hand);
#endif
                    return dev;
                    
                break;
            }
        }
    }
    return NULL;
}

uint8_t get_first_endpoint_address(libusb_device* dev)
{
    // default value
    uint8_t endpoint_address = LIBUSB_ENDPOINT_IN | LIBUSB_RECIPIENT_INTERFACE; //| LIBUSB_RECIPIENT_ENDPOINT;

    struct libusb_config_descriptor *config;
    int r = libusb_get_config_descriptor(dev, 0, &config);
    if (r == 0) {
        // assuming we have only one endpoint
        endpoint_address = config->interface[0].altsetting[0].endpoint[0].bEndpointAddress;
        libusb_free_config_descriptor(config);
    }

    #ifdef DEBUG
    printf("bEndpointAddress 0x%02x\n", endpoint_address);
    #endif

    return endpoint_address;
}
